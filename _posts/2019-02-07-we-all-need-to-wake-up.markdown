---
author: user
comments: false
date: 2019-02-07 11:56:23+00:00
layout: post
link: http://www.johnward.net/2019/02/we-all-need-to-wake-up/
slug: we-all-need-to-wake-up
title: We All Need to Wake Up
post-image: images/blogpostimage.png
categories:
- Climate
---

When the human race is extinct no one will care about Brexit, I encourage you to listen to this inspirational young lady, it puts everything into perspective.

[embed]https://www.youtube.com/watch?v=EAmmUIEsN9A&feature;=youtu.be[/embed]

I think we all need to wake up and take action.
