---
author: user
comments: true
date: 2018-07-16 09:40:39+00:00
layout: post
link: http://www.johnward.net/2018/07/sensor-fusion/
slug: sensor-fusion
title: Sensor Fusion
post-image: images/blogpostimage3.png
---

This is an old post I wrote a couple of years ago, while it is old, I still believe it is very relevant today.


### What is Sensor Fusion?


Sensor fusion is the process by which data from different data sources are fused together to detect something greater than what a single sensor could provide. One form of sensor fusion (which is more sensor enhancement) is with radio telescopes, where a very large array, known as an interferometric array, is formed from many smaller telescopes, to give the effect of being a very large radio telescope. Of course, this is giving the effect increasing the sensitivity of one kind of sensor. Sensor fusion commonly brings together many different kinds of sensors to form a complete picture of the environment in which they are situated, then uses this information to make informed decisions and implement actions as a result.


### Where is Sensor Fusion used?


The most common case at the moment is in the automotive industry, where sensor fusion is key to intelligent ADAS systems. For example, the camera, using computer vision, will detect objects and people/animals working out in front of the car. However combine this with radar and the sensor inputs to detect rotation and acceleration, and you have the basis of the car being able to make informed decisions in certain situations, for example when presented with obstacles in its path of which it is travelling. Other things such as when the car is in front of its garage door and opens it automatically, so for convenience and rather than safety critical.


### What expertise is required?


These all sounds simple but it brings a level of complexity that is unforeseen. The technology involved in such applications can be vast, as the system contains sensors with embedded MCU software, server/smart hub (or the “brain” of the system) based software collecting the information stitching it together and making decisions, all in a split second. Once the action is decided upon, then actuators and embedded software, maybe, required to implement this action. Or alternatively, this might be to email your garage to say your car needs a service.

Expertise:



 	
  * Microcontrollers

 	
  * Computer Vision

 	
  * Radar/Lidar

 	
  * Middleware Android/Linux expertise

 	
  * Hardware experience

 	
  * UI / UX expertise to relay feedback the driver safely

 	
  * Software Security




### What Further Questions might be:


So where would you find this expertise?

Who produces the hardware for this?

Who can integrate these to work as a combined solution?

Who can support this in the long term?


### Would you like a discussion on Sensor Fusion…


Please contact me for a discussion on Sensor Fusion, I am very interested in discussing your views and where these kind of technologies are heading.

Email me @Codethink: [John Ward](mailto:john.ward@codethink.com)

Email me @personal email: [John Ward](mailto:john@johnward.net)

Twitter: [@JohnWardTech](http://web.archive.org/web/20161019054200/http://www.twitter.com/@JohnWardTech)
