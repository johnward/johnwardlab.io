---
author: user
comments: true
date: 2018-07-14 21:28:13+00:00
layout: post
link: http://www.johnward.net/2018/07/introduction-to-computer-vision/
slug: introduction-to-computer-vision
title: Introduction to Computer Vision
post-image: images/blogpostimage2.png
---




## What is Computer Vision?


The field of Computer Vision allows a computer to emulate human vision by showing a perception and understanding on an image or video. This perception can then be used to allow the computer to make decisions, for example detecting the collision of an object coming towards it, or identify parts of an input image such as a face or type of object.

A computer goes through a process of analysing the images or videos information to produce numerical or symbolic data. This data is then used by accompanying computer programs to make decisions on the data.


Computer vision is developing in a number of industry areas, here are some examples below:





 	
  * Automotive industry as part of a driver assistance strategy (ADAS)

 	
  * Medical in the application of better diagnosis

 	
  * Treatment and prediction of diseases

 	
  * Entertainment systems, such as gaming




## What could Computer Vision be used for?


Uses for computer vision systems include:



 	
  * Object detection and recognition, then also segmenting the object in the area of interest.

 	
  * Image processing and filtering

 	
  * Navigation

 	
  * Organising information databases of images

 	
  * Part of a great system to detect objects and actions (i.e. a person about to walk out into the path of a moving vehicle)




## Computer Vision Examples




See my next post for computer vision examples **here** (examples coming soon!).







Mobica has excellent experience in Computer Vision, please feel free to contact me for a discussion if this is something that you need assistance with.



