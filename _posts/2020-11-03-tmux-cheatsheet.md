---
layout: post
title: "tmux Cheat Sheet"
subtitle: "A basic cheat sheet for tmux"
date: 2020-10-07 18:00:00 +0100
background: '/images/background.jpg'
author: 'John'
post-image: images/tmux.png
---
# A tmux Cheat Sheet
This is a cheat sheet for tmux, mainly for my own reference but I thought it was be useful for the general community.

## Start a new Session
` tmux new -s geek1`

## Attach to an existing session
` tmux attach-session -t geek1`

## Session Commands
* S - List sessions.
* $ - Rename current session.
* D - Detach current session.
* Ctrl+B, and then ? -  Display Help page in tmux.

## Window Commands
* C - Create a new window.
* , - Rename the current window.
* W - List the windows.
* N - Move to the next window.
* P - Move to the previous window.
* 0 to 9 - Move to the window number specified.

## Pane Commands
* % - Create a horizontal split.
* “ - Create a vertical split.
* H or Left Arrow - Move to the pane on the left.
* I or Right Arrow - Move to the pane on the right.
* J or Down Arrow - Move to the pane below.
* K or Up Arrow - Move to the pane above.
* Q - Briefly show pane numbers.
* O - Move through panes in order. Each press takes you to the next, until you loop through all of them.
* } - Swap the position of the current pane with the next.
* { - Swap the position of the current pane with the previous.
* X - Close the current pane.
