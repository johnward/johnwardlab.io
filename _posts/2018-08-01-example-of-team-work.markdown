---
author: user
comments: false
date: 2018-08-01 10:24:59+00:00
layout: post
link: http://www.johnward.net/2018/08/example-of-team-work/
slug: example-of-team-work
title: Example of Team Work
post-image: images/blogpostimage.png
---

As a cycling fan, I think grand tour cycling is the probably one of the best examples of teamwork there is. The team give everything for their main rider, which could be a sprinter or GC rider (the person riding to win overall). The actual team lead is usually not the main rider, they are doing a job, which usually goes unnoticed, to get their main rider into the winning positions throughout the race (which lasts for three weeks). Everybody has a job to do in the team and gives it their all. If they don’t give it their all the outcome is simple, the team will stand little chance of winning.

[embed]https://www.facebook.com/Eurosport/videos/how-to-win-a-sprint-caution-video-is-absolutely-mind-blowingsee-it-in-action-at-/10155009750609745/[/embed]
