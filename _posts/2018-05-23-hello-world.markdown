---
author: user
comments: true
date: 2018-05-23 10:58:43+00:00
layout: post
link: http://www.johnward.net/2018/05/hello-world/
slug: hello-world
title: Hello world again!
post-image: images/blogpostimage.png
---

My articles will be biased towards on open source subjects, but I will still work on Graphics subjects. I recently joined a new company called [Codethink](http://www.codethink.co.uk). Codethink are world leaders in Linux and Open Source.

Open Source is a interesting concept, but open source engineers tend to be a lot stronger then engineers that work on proprietary solutions because they are critiqued out in the open and by many engineers, the net result being the quality of engineering stays high.

I will also re-post some of my old articles, if they are relevant/interesting. Hope you enjoy it.
