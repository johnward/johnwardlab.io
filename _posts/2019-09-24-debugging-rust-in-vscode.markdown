---
author: user
comments: false
date: 2019-09-24 19:33:51+00:00
layout: post
link: http://www.johnward.net/2019/09/debugging-rust-in-vscode/
slug: debugging-rust-in-vscode
title: Debugging Rust in Visual Studio Code
post-image: images/blogpostimage3.png
categories:
- Rust
---

So you have written your first Rust program that is more complicated than Hello World, you are using Visual Studio Code (of course you are because it is awesome!), and you want to debug it in the IDE. Well here is what to do:



 	
  1. Enter the Debug Tab and click "_**Add Configuration**_", from the drop down next to the green play button. Select **_LLDB - Customer Launch._** You should end up with a launch.json file in your workspace.

 	
  2. Open this file and copy the text below:







{




    "version": "0.2.0",




    "configurations": [







        {




            "name": "(Linux) Launch",




            "type": "lldb",




            "request": "launch",




            "program": "${workspaceRoot}/target/debug/binary_search",




            "args": [],




            "cwd": "${workspaceRoot}",




        }




    ]




}










Change 'binary_search' to the name of your executable







You should end up with something like this:







![](http://www.johnward.net/wp-content/uploads/2019/09/vscode1.png)










3. Now you need to make sure breakpoints are on. You do thias by first going to File -> Preferences -> Settings, and in search enter 'break'. Then make sure 'Allow Breakpoints Everywhere' is ticked, see below:










![](http://www.johnward.net/wp-content/uploads/2019/09/vscode2.png)










4. Next you need to make sure it picks up the latest LLDB executable, and stop the error: 'process launch failed: unable to locate lldb-server-x.x.x'. To do this, in settings, search for 'lldb' and make sure 'Executable' just has the word **'lldb'**, remove any version number etc, see below:










![](http://www.johnward.net/wp-content/uploads/2019/09/vscode3.png)










And that is it!










You can find this project here with the vscode settings: https://github.com/johnward/algorithms_in_rust/tree/master/search_and_sorting/binary_search










Insert a breakpoint using F9 and, press F5 to debug! Have fun!
