---
author: user
comments: false
date: 2020-09-07 09:46:30+00:00
layout: post
link: http://www.johnward.net/2020/09/the-distinction-between-operating-system-kernel-bootloader-and-bios/
slug: the-distinction-between-operating-system-kernel-bootloader-and-bios
title: The distinction between Operating System, Kernel, Bootloader and BIOS
post-image: images/blogpostimage2.png
tags:
- Linux
---




Today I found this great answer on stackexchange.com, to show the difference between the Operating System, Kernel and Bootloader.







Each piece of software transitions a computer from power off to a functional system starting from the lowest level and progressing toward the software interface that you interact with. The order of execution is:







  * BIOS (Basic Input Output System) - This software is said to be close to the metal or hardware (GPU, RAM, hard drive, etc…). It runs checks on all these components and prepares them for use by the higher level software. The BIOS is firmware which is to say that it typically resides in a computer chip and not on your hard drive. It is not part of the Operating System. It does not care if you are booting Linux, Windows, or mac OS.
  * Bootloader - When the BIOS finishes it's work it looks for and passes control to the bootloader which resides on your hard drive. The bootloader then looks for bootable Kernels which are also on your hard drive (we'll get to this). It may find more than one and it typically gives you the option to choose which one you want to boot. The bootloader is part of the Operating System. The most commonly used bootloader in the Linux community is GRUB. Even though bootloaders are part of the operating system they often have the capability of booting other operating systems.
  * Kernel - The kernel handles the interface between the software that you use and the hardware resources that the software uses. It manages the use of the CPU to ensure that multiple tasks can be executed in parallel, it also deals with writing and reading data from your RAM and hard drive, the GPU for rendering to your monitor, etc…
  * Operating System - The Bootloader, Kernel, drivers that the kernel uses to talk to your specific hardware, software that is used in the background (automated backup, power management, virus scanners, etc..), and all of the software that you use in your day to day work or pleasure.






Original Post: https://elementaryos.stackexchange.com/questions/21821/distinction-between-operating-system-kernel-and-bootloader







Thank you to [Kirt Henrie](https://elementaryos.stackexchange.com/users/19632/kirt-henrie)



