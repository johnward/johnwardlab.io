---
author: user
comments: false
date: 2020-07-23 10:09:05+00:00
layout: post
link: http://www.johnward.net/2020/07/path-to-contribute-to-wayland-part-1/
slug: path-to-contribute-to-wayland-part-1
title: Path to Contribute to Wayland (Part 1)
post-image: images/blogpostimage.png
categories:
- Open Source
- Wayland
---




This is part one of my series to contribute 







After working in Open Source for the past 2 years and having gained an understanding of how FOSS projects work, I have decided it is time for me to become an open source contributor. In order to do this I needed to start by doing the following things:







  1. Decide on a technical subject that I am happy to spend my spare time doing. My Answer: Linux Display Systems, Wayland compositors  

  2. Decided which community I want to work in. My Answer: GNOME and Freedesktop  

  3. Get up to speed on the technology area (if required). For this I am implementing a basic compositor using the high level API wlroots. Follow my progress here: [https://github.com/johnward/Wardland](https://github.com/johnward/Wardland)  

  4. Join the right IRC groups, #sway-devel and #wayland on irc.freenode.net  

  5. Review the issues and select an easy issue to get you started. [https://github.com/wayland-project/weston](https://github.com/wayland-project/weston) or/and [https://github.com/GNOME/mutter](https://github.com/GNOME/mutter) or/and [https://github.com/swaywm/wlroots](https://github.com/swaywm/wlroots)






Having answered questions 1 and 2, I am now working on number 3 - writing a Wayland Compositor. This will allow me to understand wlroots and start to contribute to the project by fixing some of the issues. So far so good, watch out for part 2.



