---
author: user
comments: false
date: 2019-09-19 11:52:01+00:00
layout: post
link: http://www.johnward.net/2019/09/gnome-maintainer/
slug: gnome-maintainer
title: GNOME Maintainer
post-image: images/blogpostimage2.png
---

In August I agreed to help the GNOME foundation by becoming a maintainer for one of their projects, the game quadrapassel. I have been programming for 25 years, but have done very little in open source so thought this was a great place to roll up my sleeves and jump into the GNOME project. I would like to say a huge thank you to the GNOME community so far for being so welcoming.

Quadrapassel is a game that is a clone of the famous game Tetris, and it can be downloaded via your distributions favourite package manager.

The latest release is 3.34.0 can be found here https://download.gnome.org/sources/quadrapassel/3.34/
