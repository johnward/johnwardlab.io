---
author: user
comments: false
date: 2018-10-09 14:57:38+00:00
layout: post
link: http://www.johnward.net/2018/10/building-glfw-on-mac-os-x/
slug: building-glfw-on-mac-os-x
title: Building GLFW on Mac OS X
post-image: images/blogpostimage2.png
categories:
- Graphics
---

GLFW is a free, Open Source, multi-platform library for OpenGL, OpenGL ES and Vulkan. This platform provides a simple, API for creating windows, contexts and surfaces, reading input, handling events, etc. To allow you to provide a surface and a way to interact with your 3D graphics. There are others available such as SDL.

Dependencies:



 	
  1. Install and launch XCode from the app store.

 	
  2. Install CMake (I use brew: _**brew install cmake**_)


You could download a pre-compiled binary, however, it is better to build this on your system so it takes into account your GPU type etc.

To build GLFW from source do the following

 	
  1. Download and extract the GLFW source code and put in the location of your choice

 	
  2. Open the Terminal and cd to root the directory that GLFW has been extracted too (the one that contains

 	
  3. `cd` to the extracted directory.

 	
  4. Type: c**_make_** (hit return).

 	
  5. A Makefile will be created for you.

 	
  6. Type: _**make**_ (hit return).

 	
  7. After the compilation process, type: **_sudo __make install_**


The libraries will be copied to /usr/local/lib, and the header files copied to /usr/local/include.

So you should be good to go! Happy programming.
