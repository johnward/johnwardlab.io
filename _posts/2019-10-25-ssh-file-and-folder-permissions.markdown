---
author: user
comments: false
date: 2019-10-25 09:03:19+00:00
layout: post
link: http://www.johnward.net/2019/10/ssh-file-and-folder-permissions/
slug: ssh-file-and-folder-permissions
title: SSH File and Folder Permissions
post-image: images/blogpostimage.png
categories:
- Linux
---

Here are the file permissions of the files in your _~/.ssh_ folder, as well as the ._ssh_ folder itself.

SSH File Permissions:



 	
  * chmod 700 ~/.ssh

 	
  * chmod 644 ~/.ssh/authorized_keys

 	
  * chmod 644 ~/.ssh/known_hosts

 	
  * chmod 644 ~/.ssh/config

 	
  * chmod 600 ~/.ssh/id_rsa

 	
  * chmod 644 ~/.ssh/id_rsa.pub





