---
author: user
comments: false
date: 2020-06-12 17:16:20+00:00
layout: post
link: http://www.johnward.net/2020/06/difference-between-webgl-and-opengl/
slug: difference-between-webgl-and-opengl
title: Difference between WebGL and OpenGL
post-image: images/blogpostimage3.png
categories:
- Graphics
---




![](http://www.johnward.net/wp-content/uploads/2020/06/1200px-WebGL_Logo.svg_-2-1024x428.png)

I just wanted to write a quick post on OpenGL vs WebGL. Quite often people get hung up on which one to learn.

One of the major differences between WebGL and OpenGL is that WebGL is based on OpenGL ES so, therefore, lacks some of the features that regular OpenGL has.

One of the main features only supported in WebGL are vertex and fragment shaders, where OpenGL also has custom geometry shaders, tessellation shaders and compute shaders.

WebGL will not do the old fixed-function pipeline, which is a good thing in most cases, as it is deprecated. So by using WebGL you are forced to use buffers and shaders correctly from the very beginning.

![](http://www.johnward.net/wp-content/uploads/2020/06/opengl_logo-3.png)

WebGL and OpenGL have very similar function names and parameters, so learning WebGL you will be able to write OpenGL, as all the features in WebGL are supported in OpenGL (and vice versa).

Lastly, WebGL lacks 3D textures, instanced drawing and vertex array objects.  
So if you are learning OpenGL, as long as you are not just learning the fixed function pipeline, then translating this to WebGL should be fairly straight forward. There is no need to pick one over the other initially.









