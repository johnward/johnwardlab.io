---
author: user
comments: false
date: 2020-10-07 15:18:44+00:00
layout: post
link: http://www.johnward.net/2020/10/2020-10-07-i-wrote-a-ffmpeg-wrapper-to-convert-m4as-to-mp3s/
slug: 2020-10-07-i-wrote-a-ffmpeg-wrapper-to-convert-m4as-to-mp3s
title: I wrote a new tool ffmpeg wrapper in Rust to convert M4A's to MP3's
post-image: images/rust.png
categories:
- Rust
---


Sorry for the confusing title, but this was actually an exercise to see how quickly I could write a tool in Rust to perform a function that would typically be driven by a bash script. It turns the answer is quick, simple and easy.  

This tool is a wrapper for ffmpeg, of which I thought I would use Rust to do the directory navigation. Not the typical systems application that Rust was designed for but shows that Rust, Cargo and Rustup are very quick and handy to use. 


**Tool Description**
For longer than I care to admit I have had a 16GB SD card plugged into my infotainment system full of music. However, only a few albums were mp3, most where m4a. Strangely, the VW infotainment systems (from 2016/17) only support mp3 files. I suppose this is technology from 2012 (as automotive has a 3 to 4 lag time on this kind of technology)

So to be able to play this music I wrote a tool to convert *.m4a audio files to *.mp3. The user plugs their SD card into a laptop provides the base input directory and algorithm will recursively traverse the directory structure converting all *.m4a files to *.mp3.

The following command will start searching in the current directory and once it has converted the file, will delete the old one (without -d the old file will remain):


**_$fmtconverter -d /path/to/my/music _**

The tool is written in Rust and can be accessed here: [https://github.com/johnward/fmtconverter/blob/main/README.md](https://github.com/johnward/fmtconverter/blob/main/README.md)

Please feel free to contribute to expanding the functionality.
