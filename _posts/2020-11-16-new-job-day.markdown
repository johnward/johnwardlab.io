---
layout: post
title: "Welcome!"
subtitle: "New Job Day"
date: 2020-11-16 09:00:00 +0100
background: '/images/background.jpg'
author: 'John'
post-image: images/blogpostimage.png
---

Today I start my new job as the Head of Software for a technology company called DAI. My day job is a Head of Software role delivering software into the retail industry for smart warehousing and fullfilment. Software Engineering is what I love doing, so I will be spending my my days defining the softeware engineering processes and working on the delivery of software, as well as guiding a team to follow these processes.