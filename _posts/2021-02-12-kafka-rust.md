---
layout: post
title: "Kafka Rust Project"
subtitle: "So I have to be the new maintainer of Kafka Rust"
date: 2020-10-07 18:00:00 +0100
background: '/images/background.jpg'
author: 'John'
post-image: images/kafka-rust.png
---
# So I have to be the new maintainer of Kafka Rust
Rust lends it self to be a great solid event service, such as Kafka. When I tried to find what implementations were available I found two that need some attention. So as Kafka is getting more and more traction I decided to offer my time to help with the kafka-rust project.

The project can be found here [kafka-rust](https://github.com/spicavigo/kafka-rust), made my first commit to it this week.

# The Plan
This is my rough outline of a plan for the library, I am not going to put a timeframe on this as my day job is quite busy, plus I am still the maintainer of one of the GNOME games, which I have plans to do in Rust also! 

## Bring up to date with the latest dependencies and Rust
Get the library to build with the latest dependencies and start to tidy up the examples (i have done 1 so far and will do the rest in the next few days). This has caused the CI/CD pipeline to fail, but in my view is in a better state than it was. Perhaps I should have done this in a branch but we are where we are.

Completion of step one is to have the CI pipe line passing again with the most up to date dependencies.

## Information Gathering
Review the information from this [issue](https://github.com/spicavigo/kafka-rust/issues/185) and read the Kafka specifications 

## Deal with Pull Requests and Issues
Review all the Issues and Pull Requests and work out if they are still relevant given the exercises undertaken in parts 1 and 2.

## Dockerise Kafka itself 
This will be done so it can be deployed with one of the examples itself to allow testing of Kafka to be nice and easy. I would eventually like to get to a place where you install this lib and you have option to install Kafka itself and configure it appropriately. 

## Define a Roadmap for development
At this stage I should have enough information and knowledge of the library to define the roadmap going forward, then I can get on the with this and start the discussion in this area.

# Further reading
[Apache Kafka Project](https://kafka.apache.org/intro)