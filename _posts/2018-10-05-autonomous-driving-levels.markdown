---
author: user
comments: false
date: 2018-10-05 12:01:34+00:00
layout: post
link: http://www.johnward.net/2018/10/autonomous-driving-levels/
slug: autonomous-driving-levels
title: Autonomous Driving Levels
post-image: images/blogpostimage.png
---

Everyone has heard of autonomous cars, but did you know that most modern cars are on the autonomous driving scale, here is a simplistic overview of the levels of autonomy.

Officially, the levels of autonomy are classed into 5 levels:



 	
  * Level 1: "Hands on the wheel": The car has driver assistance functions but under the supervision of a driver. Basically, the car may apply the brakes for you etc.

 	
  * Level 2 "Hands on and off the wheel”: The car is driving itself (automated driving) but the driver is providing full supervision. This what Tesla provide at the moment, although this is creeping to Level 3.

 	
  * Level 3 “Hands off and Eyes off, but...”: This where is gets interesting, the car is driving itself (automated driving) in defined situations but **without** driver supervision, with driver is required to take the wheel if requested by the system. The System is in charge.

 	
  * Level 4 “Hands off the wheel, Eyes off and Mind Off”: The car is driving itself (automated driving) in defined situations **without** driver supervision **without** requiring the driver to take the wheel.

 	
  * Level 5 “Driverless”: The car is driving itself (automated driving)  but without the driver. This is the holy grail of autonomous driving.


