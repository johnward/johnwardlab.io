---
author: user
comments: false
date: 2018-09-19 10:28:49+00:00
layout: post
link: http://www.johnward.net/2018/09/apple-magic-mouse-with-ubuntu-speed-up-the-scrolling/
slug: apple-magic-mouse-with-ubuntu-speed-up-the-scrolling
title: Apple Magic Mouse with Ubuntu - Speed up the Scrolling
post-image: images/blogpostimage3.png
---

Recently I realised that I have a spare Apple Magic Mouse hanging round, plus it turns out that Ubuntu 16.04 comes with the Magic Mouse device driver. So to connect the mouse use the normal Bluetooth settings.

The scrolling however was a bit sluggish, so use the following commands to speed up the scrolling on the magic mouse:

**To view the current settings:**

$ systool -avm hid_magicmouse

**To change the parameters:**

$ sudo rmmod hid_magicmouse

$ sudo modprobe hid_magicmouse emulate_3button=0 scroll_acceleration=1 scroll_speed=55

**To view the change:**

$ systool -avm hid_magicmouse

Plus you should see the different when you scroll with your mouse!
