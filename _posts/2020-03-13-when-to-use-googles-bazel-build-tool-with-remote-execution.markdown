---
author: user
comments: false
date: 2020-03-13 10:45:50+00:00
layout: post
link: http://www.johnward.net/2020/03/when-to-use-googles-bazel-build-tool-with-remote-execution/
slug: when-to-use-googles-bazel-build-tool-with-remote-execution
title: When to use Google's Bazel build tool (with Remote Execution)
post-image: images/blogpostimage2.png
categories:
- Build
---




Google have been developing their internal build and test platform Blaze for multiple years, so in 2015 they decided to open source Blaze and call it [Bazel](https://bazel.build/). This was probably because build tools with similar functionality were starting to appear, such as [Please](https://please.build/) and [Buck](https://buck.build/), Google.

In March 2015 (then beta in September 2015) Google produced the initial release of the Bazel tool, which was based on Google’s Internal Blaze. Bazel is written in Java and allows for the automation of building and testing complex software, similar to tools such as Make, Ninja and Maven.

OK so there are a large number of build tools around, so why should you use Bazel (apart from the cool logo)? Well, five reasons:

* **It is fast, blazingly fast** - Bazel uses cache very intelligently, by tracking changes to both file content and build commands. It will not trigger a build if for example a source file that has comment only changes. In some cases, after migration to Bazel and Remote Execution, build times can reduce from hours to minutes. 
* **It scales incrementally well** - Bazel can manage builds of 100k+ LOC with ease, support multiple repositories, and a large number of users performing builds, sharing the artifact cache accordingly
* **Multiple platforms supported:** Linux, Mac OS and Windows.
* **Multiple Language Support: **Bazel already supports multiple languages (plus tools such as Docker) and it is designed to be extended to support any further language required as it is open source.
* **Community support: **Bazel is open source and a large number of companies are migrating to it to take advantage of the above reasons. So although with the support of Google itself, there are companies/individuals out there that can assist with full migrations or simple questions.

**Making Bazel Blaze**

* With the addition of Remote Cache and then Remote Execution this increases the speed, even more, provides the ability to compile on multiple architectures in a controlled way. So Bazel takes advantage of network-based infrastructure to perform these builds in parallel. 
* File by file building - Bazel can break down builds to a per-file basis, so it doesn’t require a whole component to be rebuilt from scratch if a line of code has changed.

Once up to speed, Bazel is a great tool for codebases with all levels of complexity, but there are times when it makes good business sense to migrate and when it doesn't. 

**When would you migrate to Bazel?**

* Your Platform with all your dependencies is under control
* Building in a supported language, on a supported platform
* Platform with high complexity, with inefficient build times (you are wasting valuable engineering time on building your software)
* A large number of developers working on the software
* You are not considering having a mix of other build tools, and have taken the strategy that migrating to a single build platform works

**When would you not migrate to Bazel? (Although a because Bazel makes things convenient it still might be worth doing)**

* Your Platform is not ready for Bazel, as it requires exact paths for dependencies and source code, not relative paths.
* Software complexity is considerably lower than 100k LOC
* Number of developers is not that high
* Although the overhead to migrate to Bazel will be not that high, so this might be a reason to migrate!

**Deploy Bazel along with a toolchain and platform **

Buildstream is a system integration tool, for the integration of software stacks, which allows you to deploy from source your toolchain and platform, along with the complex dependencies and also manage those dependencies. So when you need control of your toolchain and platform Buildstream can be used alongside Bazel so you can manage the complete software stack.

[Codethink](https://www.codethink.co.uk/integration.html) is one of the few companies in the world that have experience of deploying large scale Bazel and Remote Execution environments, so much so Google added [Codethink](https://www.linkedin.com/company/codethink-limited/) in the first small group of Bazel partners.

See the talk Daniel Silverstone did when we were at Bazelcon 2019 in December on the integration of Buildstream and Bazel to solve complex systems integration problems: [https://www.youtube.com/watch?v=21VPe7HcuPE&t=1s](https://www.youtube.com/watch?v=21VPe7HcuPE&t=1s)

If you want to discuss you build tool problems or wider technical debt I would be very interested to speak with you. Email: [john@johnward.net](http://mailto:john@johnward.net/)



