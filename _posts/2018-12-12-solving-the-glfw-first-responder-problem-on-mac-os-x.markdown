---
author: user
comments: false
date: 2018-12-12 23:27:24+00:00
layout: post
link: http://www.johnward.net/2018/12/solving-the-glfw-first-responder-problem-on-mac-os-x/
slug: solving-the-glfw-first-responder-problem-on-mac-os-x
title: Solving the GLFW First Responder Problem on Mac OS X
post-image: images/blogpostimage3.png
categories:
- Graphics
- Open Source
---

If you are running OpenGL in the XCode and getting this error message on program start-up.When you run your OpenGL program you find that you will need to resize or move the window before rendering will begin. If this is the case you are in the right place, this articles will show you how to solve it.







Error Message:




_**Setting <GLFWContentView: 0x10283a620> as the first responder for window <GLFWWindow: 0x1007396e0>, but it is in a different window ((null))! This would eventually crash when the view is freed. The first responder will be set to nil.**_







GLFW v3.2.1 causes this problem. You have probably installed glfw from brew and it has installed this version because it uses the current stable release. In my previous article, I actually show you have to install glfw using brew. But it turns out that this is not as stable as it makes out on a Mac.







So to solve the problem you need to install the master branch of GLFW, which involved downloading the source and building from scratch, which is a good thing for your system. Here are the steps to do this:







Uninstall glfw 3.2.1




($brew uninstall glfw)







Download glfw-master from [https://github.com/glfw/glfw](https://github.com/glfw/glfw) and unzip







In the root of the folder structure:







Run:




$ cmake




$ make







Output:




$ make




Scanning dependencies of target glfw




[  1%]Building C object src/CMakeFiles/glfw.dir/context.c.o




[  2%]Building C object src/CMakeFiles/glfw.dir/init.c.o




[  3%]Building C object src/CMakeFiles/glfw.dir/input.c.o




[  4%]Building C object src/CMakeFiles/glfw.dir/monitor.c.o




[  4%]Building C object src/CMakeFiles/glfw.dir/vulkan.c.o




[  5%]Building C object src/CMakeFiles/glfw.dir/window.c.o




[  6%]Building C object src/CMakeFiles/glfw.dir/cocoa_init.m.o




[  7%]Building C object src/CMakeFiles/glfw.dir/cocoa_joystick.m.o




[  8%]Building C object src/CMakeFiles/glfw.dir/cocoa_monitor.m.o




[  9%]Building C object src/CMakeFiles/glfw.dir/cocoa_window.m.o




/Users/johnward/Documents/GLFW/glfw-master/src/cocoa_window.m:1134:26:warning:




      'setWantsBestResolutionOpenGLSurface:' is deprecated: first deprecated in




      macOS 10.14 [-Wdeprecated-declarations]




        [window->ns.view setWantsBestResolutionOpenGLSurface:YES];




                         ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGLView.h:50:16: note:




      property 'wantsBestResolutionOpenGLSurface' is declared deprecated here




@property BOOL wantsBestResolutionOpenGLSurface NS_OPENGL_DEPRECATED(10_...




               ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGLView.h:50:16: note:




      'setWantsBestResolutionOpenGLSurface:' has been explicitly marked




      deprecated here




1 warning generated.




[ 10%]Building C object src/CMakeFiles/glfw.dir/cocoa_time.c.o




[ 11%]Building C object src/CMakeFiles/glfw.dir/posix_thread.c.o




[ 12%]Building C object src/CMakeFiles/glfw.dir/nsgl_context.m.o




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:39:26:warning:




      'NSOpenGLContext' is deprecated: first deprecated in macOS 10.14 - Please




      use Metal or MetalKit. [-Wdeprecated-declarations]




        [NSOpenGLContext clearCurrentContext];




                         ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:202:12: note:




      'NSOpenGLContext' has been explicitly marked deprecated here




@interface NSOpenGLContext : NSObject <NSLocking>




           ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:56:44:warning:




      'NSOpenGLContextParameterSwapInterval' is deprecated: first deprecated in




      macOS 10.14 [-Wdeprecated-declarations]




                              forParameter:NSOpenGLContextParameterSwapI...




                                           ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:177:5: note:




      'NSOpenGLContextParameterSwapInterval' has been explicitly marked




      deprecated here




    NSOpenGLContextParameterSwapInterval           NS_OPENGL_ENUM_DEPREC...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:169:5:warning:




      'NSOpenGLPixelFormatAttribute' is deprecated: first deprecated in macOS




      10.14 [-Wdeprecated-declarations]




    NSOpenGLPixelFormatAttribute attribs[40];




    ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:99:18: note:




      'NSOpenGLPixelFormatAttribute' has been explicitly marked deprecated here




typedef uint32_t NSOpenGLPixelFormatAttribute NS_OPENGL_DEPRECATED(10_0, 10_14);




                 ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:172:15:warning:




      'NSOpenGLPFAAccelerated' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




    addAttrib(NSOpenGLPFAAccelerated);




              ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:78:5: note:




      'NSOpenGLPFAAccelerated' has been explicitly marked deprecated here




    NSOpenGLPFAAccelerated           NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:173:15:warning:




      'NSOpenGLPFAClosestPolicy' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




    addAttrib(NSOpenGLPFAClosestPolicy);




              ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:79:5: note:




      'NSOpenGLPFAClosestPolicy' has been explicitly marked deprecated here




    NSOpenGLPFAClosestPolicy         NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:177:19:warning:




      'NSOpenGLPFAAllowOfflineRenderers' is deprecated: first deprecated in




      macOS 10.14 [-Wdeprecated-declarations]




        addAttrib(NSOpenGLPFAAllowOfflineRenderers);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:82:5: note:




      'NSOpenGLPFAAllowOfflineRenderers' has been explicitly marked deprecated




      here




    NSOpenGLPFAAllowOfflineRenderers NS_OPENGL_ENUM_DEPRECATED(10_5, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:190:19:warning:




      'NSOpenGLPFAOpenGLProfile' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:84:5: note:




      'NSOpenGLPFAOpenGLProfile' has been explicitly marked deprecated here




    NSOpenGLPFAOpenGLProfile         NS_OPENGL_ENUM_DEPRECATED(10_7, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:190:45:warning:




      'NSOpenGLProfileVersion4_1Core' is deprecated: first deprecated in macOS




      10.14 [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core);




                                            ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:105:5: note:




      'NSOpenGLProfileVersion4_1Core' has been explicitly marked deprecated here




    NSOpenGLProfileVersion4_1Core    NS_OPENGL_ENUM_DEPRECATED(10_10, 10...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:196:19:warning:




      'NSOpenGLPFAOpenGLProfile' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:84:5: note:




      'NSOpenGLPFAOpenGLProfile' has been explicitly marked deprecated here




    NSOpenGLPFAOpenGLProfile         NS_OPENGL_ENUM_DEPRECATED(10_7, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:196:45:warning:




      'NSOpenGLProfileVersion3_2Core' is deprecated: first deprecated in macOS




      10.14 [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core);




                                            ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:104:5: note:




      'NSOpenGLProfileVersion3_2Core' has been explicitly marked deprecated here




    NSOpenGLProfileVersion3_2Core    NS_OPENGL_ENUM_DEPRECATED(10_7, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:202:23:warning:




      'NSOpenGLPFAAuxBuffers' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




            setAttrib(NSOpenGLPFAAuxBuffers, fbconfig->auxBuffers);




                      ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:61:5: note:




      'NSOpenGLPFAAuxBuffers' has been explicitly marked deprecated here




    NSOpenGLPFAAuxBuffers            NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:214:23:warning:




      'NSOpenGLPFAAccumSize' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




            setAttrib(NSOpenGLPFAAccumSize, accumBits);




                      ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:66:5: note:




      'NSOpenGLPFAAccumSize' has been explicitly marked deprecated here




    NSOpenGLPFAAccumSize             NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:232:19:warning:




      'NSOpenGLPFAColorSize' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAColorSize, colorBits);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:62:5: note:




      'NSOpenGLPFAColorSize' has been explicitly marked deprecated here




    NSOpenGLPFAColorSize             NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:236:19:warning:




      'NSOpenGLPFAAlphaSize' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAAlphaSize, fbconfig->alphaBits);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:63:5: note:




      'NSOpenGLPFAAlphaSize' has been explicitly marked deprecated here




    NSOpenGLPFAAlphaSize             NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:239:19:warning:




      'NSOpenGLPFADepthSize' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFADepthSize, fbconfig->depthBits);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:64:5: note:




      'NSOpenGLPFADepthSize' has been explicitly marked deprecated here




    NSOpenGLPFADepthSize             NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:242:19:warning:




      'NSOpenGLPFAStencilSize' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        setAttrib(NSOpenGLPFAStencilSize, fbconfig->stencilBits);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:65:5: note:




      'NSOpenGLPFAStencilSize' has been explicitly marked deprecated here




    NSOpenGLPFAStencilSize           NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:256:19:warning:




      'NSOpenGLPFADoubleBuffer' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




        addAttrib(NSOpenGLPFADoubleBuffer);




                  ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:60:5: note:




      'NSOpenGLPFADoubleBuffer' has been explicitly marked deprecated here




    NSOpenGLPFADoubleBuffer          NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:262:23:warning:




      'NSOpenGLPFASampleBuffers' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




            setAttrib(NSOpenGLPFASampleBuffers, 0);




                      ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:69:5: note:




      'NSOpenGLPFASampleBuffers' has been explicitly marked deprecated here




    NSOpenGLPFASampleBuffers         NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:266:23:warning:




      'NSOpenGLPFASampleBuffers' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




            setAttrib(NSOpenGLPFASampleBuffers, 1);




                      ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:69:5: note:




      'NSOpenGLPFASampleBuffers' has been explicitly marked deprecated here




    NSOpenGLPFASampleBuffers         NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:267:23:warning:




      'NSOpenGLPFASamples' is deprecated: first deprecated in macOS 10.14




      [-Wdeprecated-declarations]




            setAttrib(NSOpenGLPFASamples, fbconfig->samples);




                      ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:70:5: note:




      'NSOpenGLPFASamples' has been explicitly marked deprecated here




    NSOpenGLPFASamples               NS_OPENGL_ENUM_DEPRECATED(10_0, 10_...




    ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:280:31:warning:




      'NSOpenGLPixelFormat' is deprecated: first deprecated in macOS 10.14 -




      Please use Metal or MetalKit. [-Wdeprecated-declarations]




        [[NSOpenGLPixelFormat alloc] initWithAttributes:attribs];




                              ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:113:12: note:




      'NSOpenGLPixelFormat' has been explicitly marked deprecated here




@interface NSOpenGLPixelFormat : NSObject <NSCoding>




           ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:288:5:warning:




      'NSOpenGLContext' is deprecated: first deprecated in macOS 10.14 - Please




      use Metal or MetalKit. [-Wdeprecated-declarations]




    NSOpenGLContext* share = NULL;




    ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:202:12: note:




      'NSOpenGLContext' has been explicitly marked deprecated here




@interface NSOpenGLContext : NSObject <NSLocking>




           ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:294:27:warning:




      'NSOpenGLContext' is deprecated: first deprecated in macOS 10.14 - Please




      use Metal or MetalKit. [-Wdeprecated-declarations]




        [[NSOpenGLContext alloc] initWithFormat:window->context.nsgl.pixelFormat




                          ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:202:12: note:




      'NSOpenGLContext' has been explicitly marked deprecated here




@interface NSOpenGLContext : NSObject <NSLocking>




           ^




/Users/johnward/Documents/GLFW/glfw-master/src/nsgl_context.m:307:48:warning:




      'NSOpenGLContextParameterSurfaceOpacity' is deprecated: first deprecated




      in macOS 10.14 [-Wdeprecated-declarations]




                                  forParameter:NSOpenGLContextParameterS...




                                               ^




/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AppKit.framework/Headers/NSOpenGL.h:179:5: note:




      'NSOpenGLContextParameterSurfaceOpacity' has been explicitly marked




      deprecated here




    NSOpenGLContextParameterSurfaceOpacity         NS_OPENGL_ENUM_DEPREC...




    ^




24 warnings generated.




[ 12%]Building C object src/CMakeFiles/glfw.dir/egl_context.c.o




[ 13%]Building C object src/CMakeFiles/glfw.dir/osmesa_context.c.o




[ 14%]Linking C static library libglfw3.a




[ 14%] Built target glfw




Scanning dependencies of target wave




[ 15%]Building C object examples/CMakeFiles/wave.dir/wave.c.o




[ 16%]Building C object examples/CMakeFiles/wave.dir/__/deps/glad.c.o




[ 17%]Linking C executable wave.app/Contents/MacOS/wave




Copying OS X content examples/wave.app/Contents/Resources/glfw.icns




[ 17%] Built target wave




Scanning dependencies of target simple




[ 18%]Building C object examples/CMakeFiles/simple.dir/simple.c.o




[ 19%]Building C object examples/CMakeFiles/simple.dir/__/deps/glad.c.o




[ 20%]Linking C executable simple.app/Contents/MacOS/simple




Copying OS X content examples/simple.app/Contents/Resources/glfw.icns




[ 20%] Built target simple




Scanning dependencies of target sharing




[ 21%]Building C object examples/CMakeFiles/sharing.dir/sharing.c.o




[ 21%]Building C object examples/CMakeFiles/sharing.dir/__/deps/glad.c.o




[ 22%]Linking C executable sharing.app/Contents/MacOS/sharing




Copying OS X content examples/sharing.app/Contents/Resources/glfw.icns




[ 22%] Built target sharing




Scanning dependencies of target particles




[ 23%]Building C object examples/CMakeFiles/particles.dir/particles.c.o




[ 24%]Building C object examples/CMakeFiles/particles.dir/__/deps/tinycthread.c.o




[ 25%]Building C object examples/CMakeFiles/particles.dir/__/deps/getopt.c.o




[ 26%]Building C object examples/CMakeFiles/particles.dir/__/deps/glad.c.o




[ 27%]Linking C executable particles.app/Contents/MacOS/particles




Copying OS X content examples/particles.app/Contents/Resources/glfw.icns




[ 27%] Built target particles




Scanning dependencies of target heightmap




[ 28%]Building C object examples/CMakeFiles/heightmap.dir/heightmap.c.o




[ 29%]Building C object examples/CMakeFiles/heightmap.dir/__/deps/glad.c.o




[ 29%]Linking C executable heightmap.app/Contents/MacOS/heightmap




Copying OS X content examples/heightmap.app/Contents/Resources/glfw.icns




[ 29%] Built target heightmap




Scanning dependencies of target splitview




[ 30%]Building C object examples/CMakeFiles/splitview.dir/splitview.c.o




[ 31%]Building C object examples/CMakeFiles/splitview.dir/__/deps/glad.c.o




[ 32%]Linking C executable splitview.app/Contents/MacOS/splitview




Copying OS X content examples/splitview.app/Contents/Resources/glfw.icns




[ 32%] Built target splitview




Scanning dependencies of target offscreen




[ 33%]Building C object examples/CMakeFiles/offscreen.dir/offscreen.c.o




[ 34%]Building C object examples/CMakeFiles/offscreen.dir/__/deps/glad.c.o




[ 35%]Linking C executable offscreen




[ 35%] Built target offscreen




Scanning dependencies of target gears




[ 36%]Building C object examples/CMakeFiles/gears.dir/gears.c.o




[ 37%]Building C object examples/CMakeFiles/gears.dir/__/deps/glad.c.o




[ 38%]Linking C executable gears.app/Contents/MacOS/gears




Copying OS X content examples/gears.app/Contents/Resources/glfw.icns




[ 38%] Built target gears




Scanning dependencies of target boing




[ 38%]Building C object examples/CMakeFiles/boing.dir/boing.c.o




[ 39%]Building C object examples/CMakeFiles/boing.dir/__/deps/glad.c.o




[ 40%]Linking C executable boing.app/Contents/MacOS/boing




Copying OS X content examples/boing.app/Contents/Resources/glfw.icns




[ 40%] Built target boing




Scanning dependencies of target clipboard




[ 41%]Building C object tests/CMakeFiles/clipboard.dir/clipboard.c.o




[ 42%]Building C object tests/CMakeFiles/clipboard.dir/__/deps/getopt.c.o




[ 43%]Building C object tests/CMakeFiles/clipboard.dir/__/deps/glad.c.o




[ 44%]Linking C executable clipboard




[ 44%] Built target clipboard




Scanning dependencies of target title




[ 44%]Building C object tests/CMakeFiles/title.dir/title.c.o




[ 45%]Building C object tests/CMakeFiles/title.dir/__/deps/glad.c.o




[ 46%]Linking C executable title.app/Contents/MacOS/title




[ 46%] Built target title




Scanning dependencies of target timeout




[ 47%]Building C object tests/CMakeFiles/timeout.dir/timeout.c.o




[ 48%]Building C object tests/CMakeFiles/timeout.dir/__/deps/glad.c.o




[ 49%]Linking C executable timeout.app/Contents/MacOS/timeout




[ 49%] Built target timeout




Scanning dependencies of target threads




[ 50%]Building C object tests/CMakeFiles/threads.dir/threads.c.o




[ 51%]Building C object tests/CMakeFiles/threads.dir/__/deps/tinycthread.c.o




[ 52%]Building C object tests/CMakeFiles/threads.dir/__/deps/glad.c.o




[ 53%]Linking C executable threads.app/Contents/MacOS/threads




[ 53%] Built target threads




Scanning dependencies of target opacity




[ 54%]Building C object tests/CMakeFiles/opacity.dir/opacity.c.o




[ 55%]Building C object tests/CMakeFiles/opacity.dir/__/deps/glad.c.o




[ 55%]Linking C executable opacity.app/Contents/MacOS/opacity




[ 55%] Built target opacity




Scanning dependencies of target gamma




[ 55%]Building C object tests/CMakeFiles/gamma.dir/gamma.c.o




[ 56%]Building C object tests/CMakeFiles/gamma.dir/__/deps/glad.c.o




[ 57%]Linking C executable gamma.app/Contents/MacOS/gamma




[ 57%] Built target gamma




Scanning dependencies of target tearing




[ 58%]Building C object tests/CMakeFiles/tearing.dir/tearing.c.o




[ 58%]Building C object tests/CMakeFiles/tearing.dir/__/deps/glad.c.o




[ 59%]Linking C executable tearing.app/Contents/MacOS/tearing




[ 59%] Built target tearing




Scanning dependencies of target iconify




[ 60%]Building C object tests/CMakeFiles/iconify.dir/iconify.c.o




[ 61%]Building C object tests/CMakeFiles/iconify.dir/__/deps/getopt.c.o




[ 62%]Building C object tests/CMakeFiles/iconify.dir/__/deps/glad.c.o




[ 63%]Linking C executable iconify




[ 63%] Built target iconify




Scanning dependencies of target joysticks




[ 64%]Building C object tests/CMakeFiles/joysticks.dir/joysticks.c.o




[ 65%]Building C object tests/CMakeFiles/joysticks.dir/__/deps/glad.c.o




[ 66%]Linking C executable joysticks.app/Contents/MacOS/joysticks




[ 66%] Built target joysticks




Scanning dependencies of target windows




[ 67%]Building C object tests/CMakeFiles/windows.dir/windows.c.o




[ 68%]Building C object tests/CMakeFiles/windows.dir/__/deps/getopt.c.o




[ 69%]Building C object tests/CMakeFiles/windows.dir/__/deps/glad.c.o




[ 70%]Linking C executable windows.app/Contents/MacOS/windows




[ 70%] Built target windows




Scanning dependencies of target inputlag




[ 71%]Building C object tests/CMakeFiles/inputlag.dir/inputlag.c.o




[ 72%]Building C object tests/CMakeFiles/inputlag.dir/__/deps/getopt.c.o




[ 72%]Building C object tests/CMakeFiles/inputlag.dir/__/deps/glad.c.o




[ 73%]Linking C executable inputlag.app/Contents/MacOS/inputlag




[ 73%] Built target inputlag




Scanning dependencies of target empty




[ 74%]Building C object tests/CMakeFiles/empty.dir/empty.c.o




[ 75%]Building C object tests/CMakeFiles/empty.dir/__/deps/tinycthread.c.o




[ 76%]Building C object tests/CMakeFiles/empty.dir/__/deps/glad.c.o




[ 77%]Linking C executable empty.app/Contents/MacOS/empty




[ 77%] Built target empty




Scanning dependencies of target reopen




[ 78%]Building C object tests/CMakeFiles/reopen.dir/reopen.c.o




[ 79%]Building C object tests/CMakeFiles/reopen.dir/__/deps/glad.c.o




[ 80%]Linking C executable reopen




[ 80%] Built target reopen




Scanning dependencies of target msaa




[ 80%]Building C object tests/CMakeFiles/msaa.dir/msaa.c.o




[ 81%]Building C object tests/CMakeFiles/msaa.dir/__/deps/getopt.c.o




[ 82%]Building C object tests/CMakeFiles/msaa.dir/__/deps/glad.c.o




[ 83%]Linking C executable msaa




[ 83%] Built target msaa




Scanning dependencies of target cursor




[ 84%]Building C object tests/CMakeFiles/cursor.dir/cursor.c.o




[ 85%]Building C object tests/CMakeFiles/cursor.dir/__/deps/glad.c.o




[ 85%]Linking C executable cursor




[ 85%] Built target cursor




Scanning dependencies of target glfwinfo




[ 86%]Building C object tests/CMakeFiles/glfwinfo.dir/glfwinfo.c.o




[ 87%]Building C object tests/CMakeFiles/glfwinfo.dir/__/deps/getopt.c.o




[ 88%]Building C object tests/CMakeFiles/glfwinfo.dir/__/deps/glad.c.o




[ 89%]Linking C executable glfwinfo




[ 89%] Built target glfwinfo




Scanning dependencies of target monitors




[ 90%]Building C object tests/CMakeFiles/monitors.dir/monitors.c.o




[ 91%]Building C object tests/CMakeFiles/monitors.dir/__/deps/getopt.c.o




[ 92%]Building C object tests/CMakeFiles/monitors.dir/__/deps/glad.c.o




[ 93%]Linking C executable monitors




[ 93%] Built target monitors




Scanning dependencies of target events




[ 94%]Building C object tests/CMakeFiles/events.dir/events.c.o




[ 95%]Building C object tests/CMakeFiles/events.dir/__/deps/getopt.c.o




[ 96%]Building C object tests/CMakeFiles/events.dir/__/deps/glad.c.o




[ 97%]Linking C executable events




[ 97%] Built target events




Scanning dependencies of target icon




[ 98%]Building C object tests/CMakeFiles/icon.dir/icon.c.o




[ 99%]Building C object tests/CMakeFiles/icon.dir/__/deps/glad.c.o




[100%]Linking C executable icon.app/Contents/MacOS/icon




[100%] Built target icon










$ sudo make install




You should get something like below as output:







$ sudo make install




Password:




[ 14%] Built target glfw




[ 17%] Built target wave




[ 20%] Built target simple




[ 22%] Built target sharing




[ 27%] Built target particles




[ 29%] Built target heightmap




[ 32%] Built target splitview




[ 35%] Built target offscreen




[ 38%] Built target gears




[ 40%] Built target boing




[ 44%] Built target clipboard




[ 46%] Built target title




[ 49%] Built target timeout




[ 53%] Built target threads




[ 55%] Built target opacity




[ 57%] Built target gamma




[ 59%] Built target tearing




[ 63%] Built target iconify




[ 66%] Built target joysticks




[ 70%] Built target windows




[ 73%] Built target inputlag




[ 77%] Built target empty




[ 80%] Built target reopen




[ 83%] Built target msaa




[ 85%] Built target cursor




[ 89%] Built target glfwinfo




[ 93%] Built target monitors




[ 97%] Built target events




[100%] Built target icon




Install the project...




-- Install configuration: ""




-- Installing: /usr/local/include/GLFW




-- Installing: /usr/local/include/GLFW/glfw3.h




-- Installing: /usr/local/include/GLFW/glfw3native.h




-- Installing: /usr/local/lib/cmake/glfw3/glfw3Config.cmake




-- Installing: /usr/local/lib/cmake/glfw3/glfw3ConfigVersion.cmake




-- Installing: /usr/local/lib/cmake/glfw3/glfw3Targets.cmake




-- Installing: /usr/local/lib/cmake/glfw3/glfw3Targets-noconfig.cmake




-- Installing: /usr/local/lib/pkgconfig/glfw3.pc




-- Installing: /usr/local/lib/libglfw3.a




Johns-MacBook:glfw-master johnward$







This should have built the library and installed it in the directories above.







Then in your Xcode library







Add Libraries:




libglfw3.a (from /usr/local/lib folder)







You also need:




CoreVideo.framework




IOKit.framework




Cocoa.framework




OpenGL.framework







Then the project should build
