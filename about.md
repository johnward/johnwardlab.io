---
title: About
layout: page
---

# Hello!
My name is John Ward, day-to-day I’m CTO. I’m also a Open Source Maintainer, 3D Graphics and Rust programmer.

My day job is CTO working with a high tech company called SamsonVT. Software Engineering is what I love doing, as I spent many years as a Software Engineer / Project Manager / Technical Lead / Group Manager, LinkedIn Profile [here](https://www.linkedin.com/in/wardjp/), working in the telecommunications/semiconductor / automotive industries, working on exciting projects. Here is my [Github](https://github.com/johnward).

I have been programming since the ZX Spectrum (+2 with Cassette!- not the original) and BBC Micro was first released.

I also worked on one of the very first smartphones the Nokia 6600 and Nokia 7610.

## I can help you with:
* Building team to deliver distributed cloud based applications, using Kubernetes and container technologies such as Docker
* Porting applications to Rust
* Rust Microservices with Kafka, deployed onto Docker and Kubernetes
* Programme and Project Management, Technical Leading – general software project delivery
* Making your software building faster remove the bottlenecks
* Build a Sales Strategy and grow your company
* Improving or creating Computer Graphics, for example with mainly OpenGL but also familier with DirectX, Vulcan etc.
* Display stack architecture improvements

Away from the office, you will find me spending time with my family, on a windy beach kitesurfing, paddleboarding, or out on my bike heading up a mountain road or trail.
