# John Ward's Website
Welcome to John Ward's website

![Profile](assets/images/profile.png)

# About Me
My name is John Ward, day-to-day I'm the CTO for SamsonVT. I’m also a Software Engineer, Open Source Maintainer and 3d Graphics  Programmer. My day job is the CTO, managing teams of software engineers to deliver high quality software. Software Engineering is what I love doing, as I spent many years as a Software Engineer / Project Manager / Technical Lead / Group Manager (LinkedIn Profile [here][linkedin]) working in the telecommunications/semiconductor / automotive industries, working on exciting projects. Here is my [Github][github]. When I am in the office (pre-COVID), I am based right in Cheadle with DAI, who are part of the thriving technology scene. I have been programming since the ZX Spectrum (+2 with Cassette!- not the original) and BBC Micro was first released. I also worked on one of the very first smartphones the Nokia 6600 and Nokia 7610.